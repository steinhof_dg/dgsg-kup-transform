<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="3.0">
    <!-- COPY -->
    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="attribute() | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>
    <!-- REMOVE -->
    <xsl:template match="params"/>
    <xsl:template match="serial[@is_bibliographic='N']"/>
    <xsl:template match="product[@is_set='Y'][not(medium='BU')]"/>
    <xsl:template match="project_no_edition"/>
    <xsl:template match="reprint_type"/>
    <xsl:template match="publishing_house_dept"/>
    <xsl:template match="copyright_holders/text"/>
    <xsl:template match="copyright_holders/copyright_holder/text"/>
    <xsl:template match="copyright_holder[@mention='N']" priority="2"/>
    <xsl:template match="trim_size"/>
    <xsl:template match="first_planned_pub_date"/>
    <xsl:template match="first_pub_date"/>
    <xsl:template match="first_shipping_date"/>
    <xsl:template match="print_date"/>
    <xsl:template match="commodity_code_duty"/>
    <xsl:template match="classifications/category[@code = 'ABI']"/>
    <xsl:template match="classifications/category[@code = 'AOKOMU']"/>
    <xsl:template match="classifications/category[@code = 'APE']"/>
    <xsl:template match="classifications/category[@code = 'APP']"/>
    <xsl:template match="classifications/category[@code = 'BGR']"/>
    <xsl:template match="classifications/category[@code = 'BIRKBACK']"/>
    <xsl:template match="classifications/category[@code = 'BLAT']"/>
    <xsl:template match="classifications/category[@code = 'BOD']"/>
    <xsl:template match="classifications/category[@code = 'BST']"/>
    <xsl:template match="classifications/category[@code = 'BUCH']"/>
    <xsl:template match="classifications/category[@code = 'BULK']"/>
    <xsl:template match="classifications/category[@code = 'BZAEA']"/>
    <xsl:template match="classifications/category[@code = 'CHP']"/>
    <xsl:template match="classifications/category[@code = 'COR52']"/>
    <xsl:template match="classifications/category[@code = 'COVER']"/>
    <xsl:template match="classifications/category[@code = 'CP']"/>
    <xsl:template match="classifications/category[@code = 'CPI']"/>
    <xsl:template match="classifications/category[@code = 'CR']"/>
    <xsl:template match="classifications/category[@code = 'DB']"/>
    <xsl:template match="classifications/category[@code = 'DDB']"/>
    <xsl:template match="classifications/category[@code = 'DES']"/>
    <xsl:template match="classifications/category[@code = 'DGS']"/>
    <xsl:template match="classifications/category[@code = 'DOLL']"/>
    <xsl:template match="classifications/category[@code = 'EMW']"/>
    <xsl:template match="classifications/category[@code = 'EXP']"/>
    <xsl:template match="classifications/category[@code = 'FB']"/>
    <xsl:template match="classifications/category[@code = 'FL']"/>
    <xsl:template match="classifications/category[@code = 'FN']"/>
    <xsl:template match="classifications/category[@code = 'FRB']"/>
    <xsl:template match="classifications/category[@code = 'FRC']"/>
    <xsl:template match="classifications/category[@code = 'FSL']"/>
    <xsl:template match="classifications/category[@code = 'HUCO']"/>
    <xsl:template match="classifications/category[@code = 'ID']"/>
    <xsl:template match="classifications/category[@code = 'JA']"/>
    <xsl:template match="classifications/category[@code = 'KE']"/>
    <xsl:template match="classifications/category[@code = 'KEPUB']"/>
    <xsl:template match="classifications/category[@code = 'KLA']"/>
    <xsl:template match="classifications/category[@code = 'KON']"/>
    <xsl:template match="classifications/category[@code = 'LSI']"/>
    <xsl:template match="classifications/category[@code = 'LPME']"/>
    <xsl:template match="classifications/category[@code = 'LUC']"/>
    <xsl:template match="classifications/category[@code = 'LZ']"/>
    <xsl:template match="classifications/category[@code = 'MKGMODE']"/>
    <xsl:template match="classifications/category[@code = 'MNV']"/>
    <xsl:template match="classifications/category[@code = 'ND']"/>
    <xsl:template match="classifications/category[@code = 'NL']"/>
    <xsl:template match="classifications/category[@code = 'NOEE']"/>    
    <xsl:template match="classifications/category[@code = 'NONDES']"/>
    <xsl:template match="classifications/category[@code = 'NOTI']"/>
    <xsl:template match="classifications/category[@code = 'ONTOS']"/>
    <xsl:template match="classifications/category[@code = 'PP']"/>
    <xsl:template match="classifications/category[@code = 'PRAA']"/>
    <xsl:template match="classifications/category[@code = 'PRD']"/>
    <xsl:template match="classifications/category[@code = 'RLS']"/>
    <xsl:template match="classifications/category[@code = 'RPOD']"/>
    <xsl:template match="classifications/category[@code = 'RS']"/>
    <xsl:template match="classifications/category[@code = 'SG']"/>
    <xsl:template match="classifications/category[@code = 'SIG']"/>
    <xsl:template match="classifications/category[@code = 'SKAL']"/>
    <xsl:template match="classifications/category[@code = 'SM']"/>
    <xsl:template match="classifications/category[@code = 'SZH']"/>
    <xsl:template match="classifications/category[@code = 'TECH']"/>
    <xsl:template match="classifications/category[@code = 'TEUB']"/>
    <xsl:template match="classifications/category[@code = 'TIS']"/>
    <xsl:template match="classifications/category[@code = 'TM']"/>
    <xsl:template match="classifications/category[@code = 'TOP']"/>
    <xsl:template match="classifications/category[@code = 'TSB']"/>
    <xsl:template match="classifications/category[@code = 'VA']"/>
    <xsl:template match="classifications/category[@code = 'VST']"/>
    <xsl:template match="classifications/category[@code = 'VT']"/>
    <xsl:template match="classifications/category[@code = 'WERB']"/>      
    <xsl:template match="classifications/category[@code = 'WP']"/>
    <xsl:template match="content/text[@text_type = 'FOAD']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'FOAE']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'KZSD']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'KZSE']" priority="200"/>
    <xsl:template match="content/text[@text_type = '']"/>
    <!-- Maling Vertrieb -->
    <xsl:template match="content/text[@text_type = 'BUD']" priority="200"/>
    <!--<xsl:template match="content/text[@text_type = 'BUE']" priority="200"/> FOR ONIX -->
    <xsl:template match="content/text[@text_type = 'BUDK']"/>
    <xsl:template match="content/text[@text_type = 'BUEK']"/>
    <xsl:template match="content/text[@text_type = 'BEM']"/>
    <xsl:template match="content/text[@text_type = 'FUSSN']"/>
    <xsl:template match="content/text[@text_type = 'SFORM']"/>
    <!--<xsl:template match="content/text[@text_type = 'INH']"/> FOR ONIX -->
    <xsl:template match="content/text[@text_type = 'IP']"/>
    <xsl:template match="content/text[@text_type = 'KLTX']"/>
    <xsl:template match="content/text[@text_type = 'TITEL1']"/>
    <xsl:template match="content/text[@text_type = 'FUSSW']"/>
    <xsl:template match="content/text[@text_type = '']"/>
    <!-- SaurMNV Archiv.Info -->
    <xsl:template match="content/text[@text_type = 'BIBD']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'BIBE']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'BUE2']"/>
    <xsl:template match="content/text[@text_type = '']"/>
    <!-- EZ  -->
    <xsl:template match="content/text[@text_type = 'BACKMAT']"/>
    <xsl:template match="content/text[@text_type = 'CHBRDOWN']"/>
    <xsl:template match="content/text[@text_type = 'FRONTMAT']"/>
    <xsl:template match="content/text[@text_type = 'TNVD']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'TNVE']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'WEBD']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'WEBE']" priority="200"/>
    <xsl:template match="content/text[@text_type = 'BCC']"/>
    <xsl:template match="content/text[@text_type = 'VORBDP']"/>
    <xsl:template match="content/text[@text_type = 'VORBEP']"/>
    <xsl:template match="content/text[@text_type = 'KYW']"/>
    <xsl:template match="content/text[@text_type = 'PART']"/>
    <xsl:template match="content/text[@text_type = 'PARTU']"/>
    <xsl:template match="content/text[@text_type = 'TITEL2']"/>
    <xsl:template match="content/text[@text_type = 'ALT']"/>
    <xsl:template match="content/text[@text_type = 'INT']"/>
    <xsl:template match="content/text[@text_type = 'DOI URL']"/>
    <xsl:template match="content/text[@text_type = '']"/>
    <!-- FM  -->
    <xsl:template match="comments"/>
    <xsl:template match="linked_objects/link[@term = 'Text']"/>
    <xsl:template match="linked_objects/link[@term = 'Vertragsdokument']"/>
    <xsl:template match="linked_objects/link[@term = 'Online Content']"/>
    <xsl:template match="linked_objects/link[@term = 'Probeheft']"/>
    <xsl:template match="linked_objects/link[@term = 'TOC-Alerting']"/>
    <xsl:template match="linked_objects/link[@term = 'Open Access']"/>
    <xsl:template match="linked_objects/link[@term = 'Hinw.EinzArt.-Bezug']"/>
    <xsl:template match="linked_objects/link[@term = 'Onl.Subm.of Manuscripts']"/>
    <xsl:template match="linked_objects/link[@term = 'MVB Rights Link']"/>
    <xsl:template match="linked_objects/link[@term = 'Pod-/Vidcast']"/>
    <xsl:template match="linked_objects/link[@term = 'Transmittal']"/>
    <xsl:template match="linked_objects/link[@term = 'BookFiles']"/>
    <xsl:template match="catchwords[not(subject_word[contains(@type, '+vlb')])][not(parent::product/content/text[@text_type = 'KYW'])]"/>
    <xsl:template match="catchwords/keyword"/>
    <xsl:template match="catchwords/subject_word[not(contains(@type, '+vlb'))]"/>
    <xsl:template match="responsible_persons"/>
    <xsl:template match="print_run"/>
    <!--<xsl:template match="distribution_rights"/> FOR ONIX -->
    <xsl:template match="deliver_remaining_stock"/>
    <xsl:template match="title_registration"/>
    <xsl:template match="bibl_remark"/>
    <xsl:template match="schedules"/>
    <!-- overwrite pub_date and planned_pub_date with external pub date classification if present -->
    <xsl:template match="pub_date | planned_pub_date">
        <xsl:variable name="date" select="name(.)"/>
        <xsl:element name="{$date}">
            <xsl:choose>
                <xsl:when test="parent::product/classifications/category[@code = 'EPD']">
                    <xsl:value-of select="replace(parent::product/classifications/category[@code = 'EPD']/@value, '(\d\d)\.(\d\d)\.(\d\d\d\d)', '$3-$2-$1')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    <!-- combine german and english keywords -->
    <xsl:template match="catchwords">
        <xsl:element name="catchwords">
            <xsl:apply-templates/>
            <xsl:for-each select="
                (for $token in tokenize(parent::product/content/text[@text_type = 'KYW'][1], '(; )|(;)|(, )|(,)')
                    return
                        $token)">
                <xsl:element name="subject_word">
                    <xsl:attribute name="xml:lang" select="'en'"/>
                    <xsl:attribute name="type" select="'+vlb'"/>
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
    <xsl:template match="catchwords/subject_word[contains(@type, '+vlb')]">
        <xsl:element name="subject_word">
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:attribute name="type" select="@type"/>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
     <!-- REMOVE non bibliographic series that are not a package, but keep the e-book package relations  -->
    <xsl:template match="serial_relation[@is_bibliographic = 'N'][not(@is_set)]"/>
    <!-- skip the price types we don't need -->
    <xsl:template match="price[not(price_type=('SUSE1','SUSE3','BL','SPL','LP','EM','SER','DAB','TEP','IFCP'))]"/>  
    <xsl:template match="price[price_type=('SUSE1','SUSE3') and (parent::prices/preceding-sibling::cover='EPUB')]" priority="2">
        <xsl:message>remove SUSE from epub</xsl:message>
    </xsl:template>
    <!-- set shop or display web-type -->
    <xsl:template match="price">
        <xsl:choose>
            <xsl:when test="parent::prices/price/price_type=('SUSE1','SUSE3') and (price_type=('SPL','LP','EM'))">
                <xsl:message>remove price because there is a SUSE price</xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{name()}">
                    <xsl:copy-of select="@*"/>
                    <xsl:attribute name="web-type">
                        <xsl:choose>
                            <xsl:when test="price_type = ('SUSE1','SUSE3','SPL','LP','EM')">shop</xsl:when>
                            <xsl:when test="price_type = ('IFCP')">chunk</xsl:when>
                            <xsl:otherwise>display</xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:copy-of select="*"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- combine texts add xml:lang attribute -->
    <!-- duplicate format for each language -->
    <xsl:template match="format[@term]">
        <format term="{@term} cm" xml:lang="de">
            <xsl:value-of select="."/>
        </format>
        <format term="{replace(@term,',','.')} cm" xml:lang="en">
            <xsl:value-of select="."/>
        </format>
    </xsl:template>
    <xsl:template match="content">
        <xsl:element name="{name()}">
            <!-- combine several product text into one -->
            <xsl:if test="text[@text_type = ('LTXD', 'CONT1', 'KTXD')]">
                <xsl:element name="text">
                    <xsl:attribute name="text_status" select="'OPEN'"/>
                    <xsl:attribute name="text_type" select="'TXT'"/>
                    <xsl:attribute name="term" select="'Produkttext deutsch'"/>
                    <xsl:attribute name="xml:lang" select="'de'"/>
                    <xsl:choose>
                        <xsl:when test="text[@text_type = 'LTXD']">
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'LTXD'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT3'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                        </xsl:when>
                        <xsl:when test="text[@text_type = 'CONT1']">
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT1'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT3'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'KTXD'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT3'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </xsl:if>
            <xsl:if test="text[@text_type = ('LTXE', 'CONT2', 'KTXE')]">
                <xsl:element name="text">
                    <xsl:attribute name="text_status" select="'OPEN'"/>
                    <xsl:attribute name="text_type" select="'TXT'"/>
                    <xsl:attribute name="term" select="'Produkttext englisch'"/>
                    <xsl:attribute name="xml:lang" select="'en'"/>
                    <xsl:choose>
                        <xsl:when test="text[@text_type = 'LTXE']">
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'LTXE'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT3'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                        </xsl:when>
                        <xsl:when test="text[@text_type = 'CONT2']">
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT2'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT3'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'KTXE'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;', '$1')"/>
                            <xsl:value-of select="replace(normalize-space(text[@text_type = 'CONT3'][1]), '.*&lt;body&gt;(.*)&lt;/body&gt;.*', '$1')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates/>
            <!-- move product/bibl_remark to content/text[@text_type='BH'] -->
            <xsl:if test="parent::product/bibl_remark">
                <xsl:element name="text">
                    <xsl:attribute name="text_status" select="'OPEN'"/>
                    <xsl:attribute name="text_type" select="'BH'"/>
                    <xsl:attribute name="term" select="'Bibl. Hinweis deutsch'"/>
                    <xsl:attribute name="xml:lang" select="'de'"/>
                    <xsl:copy-of select="parent::product/bibl_remark/node()"/>
                </xsl:element>
            </xsl:if>
        </xsl:element>
    </xsl:template>
    <xsl:template match="content/text[ends-with(@text_type, 'D')] | content/text[ends-with(@text_type, 'E')]">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="substring(@text_type, 1, string-length(@text_type) - 1)"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang">
                <xsl:choose>
                    <xsl:when test="ends-with(@text_type, 'D')">de</xsl:when>
                    <xsl:when test="ends-with(@text_type, 'E')">en</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="content/text[@text_type = 'ZIELG'][1] | content/text[@text_type = 'UEZ'][1]">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'ZIELG'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang">
                <xsl:choose>
                    <xsl:when test="@text_type = 'ZIELG'">de</xsl:when>
                    <xsl:when test="@text_type = 'UEZ'">en</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="content/text[@text_type = 'DIRI'][1] | content/text[@text_type = 'ETI'][1] | content/text[@text_type = 'BEIGE'][1] " priority="200">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:copy-of select="@text_type"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="content/text[@text_type = 'CONT1'][1] | content/text[@text_type = 'CONT2'][1]" priority="200">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'CONT'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang">
                <xsl:choose>
                    <xsl:when test="@text_type = 'CONT1'">de</xsl:when>
                    <xsl:when test="@text_type = 'CONT2'">en</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="content/text[@text_type = 'HINWEIS'][1] | content/text[@text_type = 'HINE'][1]" priority="200">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'HIN'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang">
                <xsl:choose>
                    <xsl:when test="@text_type = 'HINWEIS'">de</xsl:when>
                    <xsl:when test="@text_type = 'HINE'">en</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="content/text[@text_type = 'SINFO'][1] | content/text[@text_type = 'RIE'][1]" priority="200">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'RIE'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang">
                <xsl:choose>
                    <xsl:when test="@text_type = 'SINFO'">de</xsl:when>
                    <xsl:when test="@text_type = 'RIE'">en</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>