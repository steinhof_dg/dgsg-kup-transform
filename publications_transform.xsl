<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:strip-space elements="prices collaborator classifications"/>
    <!-- COPY -->
    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="attribute() | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>
    <!-- REMOVE -->
    <xsl:template match="params"/>
    <xsl:template match="edition[pub_status = 'SUSP']"/>
    <xsl:template match="status"/>
    <xsl:template match="reference_key"/>
    <xsl:template match="planning_division"/>
    <xsl:template match="resp_planner"/>
    <xsl:template match="order_notice"/>
    <xsl:template match="classifications/category[@code = 'BEPRE']"/>
    <xsl:template match="classifications/category[@code = 'BIC']"/>
    <xsl:template match="classifications/category[@code = 'BZAEA']"/>
    <xsl:template match="classifications/category[@code = 'CHP']"/>
    <xsl:template match="classifications/category[@code = 'DGM']"/>
    <xsl:template match="classifications/category[@code = 'DGS']"/>
    <xsl:template match="classifications/category[@code = 'DOLL']"/>
    <xsl:template match="classifications/category[@code = 'EXP']"/>
    <xsl:template match="classifications/category[@code = 'FB']"/>
    <xsl:template match="classifications/category[@code = 'FL']"/>
    <xsl:template match="classifications/category[@code = 'FN']"/>
    <xsl:template match="classifications/category[@code = 'JG']"/>
    <xsl:template match="classifications/category[@code = 'LUC']"/>
    <xsl:template match="classifications/category[@code = 'MKGMODE']"/>
    <xsl:template match="classifications/category[@code = 'NEA']"/>
    <xsl:template match="classifications/category[@code = 'PRD']"/>
    <xsl:template match="classifications/category[@code = 'PTS']"/>
    <xsl:template match="classifications/category[@code = 'SG']"/>
    <xsl:template match="classifications/category[@code = 'SM']"/>
    <xsl:template match="classifications/category[@code = 'VES']"/>
    <xsl:template match="classifications/category[@code = 'VS']"/>
    <xsl:template match="classifications/category[@code = 'VTSERIE']"/>
    <xsl:template match="classifications/category[@code = 'WP']"/>
    <xsl:template match="standard_dispatch"/>
    <xsl:template match="standard_dispatch_foreign"/>
    <xsl:template match="internal_remarks"/>
    <xsl:template match="postages"/>
    <xsl:template match="vat_rates"/>
    <xsl:template match="suspension"/>
    <xsl:template match="histories"/>
    <xsl:template match="manufacture"/>
    <xsl:template match="contents/text[@text_type = 'MITD']"/>
    <xsl:template match="contents/text[@text_type = 'MITE']"/>
    <xsl:template match="contents/text[@text_type = 'CFPD']"/>
    <xsl:template match="contents/text[@text_type = 'CFPE']"/>
    <xsl:template match="contents/text[@text_type = 'AUID']"/>
    <xsl:template match="contents/text[@text_type = 'AUIE']"/>
    <xsl:template match="contents/text[@text_type = 'ZUSD']"/>
    <xsl:template match="contents/text[@text_type = 'ZUSE']"/>
    <xsl:template match="contents/text[@text_type = 'BIRE']"/>
    <xsl:template match="contents/text[@text_type = 'ECAG']"/>
    <xsl:template match="contents/text[@text_type = 'ECAE']"/>
    <xsl:template match="contents/text[@text_type = 'FREE']"/>
    <xsl:template match="contents/text[@text_type = 'VOBDJ']"/>
    <xsl:template match="contents/text[@text_type = 'VOBEJ']"/>
    <xsl:template match="linked_objects/link[@term = 'Bild']"/>
    <xsl:template match="linked_objects/link[@term = 'Text']"/>
    <xsl:template match="linked_objects/link[@term = 'Vertragsdokument']"/>
    <xsl:template match="linked_objects/link[@term = 'Online Content']"/>
    <xsl:template match="linked_objects/link[@term = 'Probeheft']"/>
    <xsl:template match="linked_objects/link[@term = 'TOC-Alerting']"/>
    <xsl:template match="linked_objects/link[@term = 'Open Access']"/>
    <xsl:template match="linked_objects/link[@term = 'Hinw.EinzArt.-Bezug']"/>
    <xsl:template match="linked_objects/link[@term = 'MVB Rights Link']"/>
    <xsl:template match="linked_objects/link[@term = 'Hardcover available from HUP']"/>
    <xsl:template match="linked_objects/link[@term = 'Paperback available from HUP']"/>
    <xsl:template match="linked_objects/link[@term = 'Pod-/Vidcast']"/>
    <xsl:template match="linked_objects/link[@term = 'DOI URL']"/>
    <xsl:template match="linked_objects/link[@term = 'Transmittal']"/>
    <xsl:template match="linked_objects/link[@term = 'BookFiles']"/>
    <xsl:template match="bundle_price_determination"/>
    <xsl:template match="document_output"/>
    <xsl:template match="collaborator/institution_name"/>
    <xsl:template match="collaborator/department"/>
    <xsl:template match="collaborator/sub_department"/>
    <xsl:template match="collaborator/street"/>
    <xsl:template match="collaborator/street_no"/>
    <xsl:template match="collaborator/postal_code"/>
    <xsl:template match="collaborator/city"/>
    <xsl:template match="collaborator/country"/>
    <xsl:template match="collaborator/email"/>
    <xsl:template match="collaborator/phone_number"/>
    <xsl:template match="collaborator/fax_number"/>
    <xsl:template match="collaborator/corresponding_author"/>
    <xsl:template match="collaborator/date_valid_from"/>
    <!-- remove bundles -->
    <xsl:template match="@is_bundle[. = 'Y']">
        <xsl:attribute name="is_bundle">N</xsl:attribute>
    </xsl:template>
    <xsl:template match="bundle_items"/>
    <xsl:template match="journal_key">
        <xsl:variable name="jkey" select="."/>
        <xsl:element name="{name()}">
            <xsl:choose>
                <xsl:when test="parent::edition/classifications/category[@code = 'DBLINK']">
                    <xsl:value-of select="parent::edition/classifications/category[@code = 'DBLINK']/@value"/>
                </xsl:when>
                <xsl:when test="doc('kup_code_mapping.xml')/root/row/kup_code = $jkey">
                    <xsl:value-of select="upper-case(doc('kup_code_mapping.xml')/root/row[kup_code = $jkey]/online_code)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    <xsl:template match="prices[parent::edition/classifications/category[@code = ('OA', 'FA')]]"/>
    <xsl:template match="prices[parent::edition/classifications/category[not(category/@code='OPACSYST-CHUNK')]/category[@code = 'OPACSYST-EFF'][xs:date(concat(substring(@value, 7, 4), '-', substring(@value, 4, 2), '-', substring(@value, 1, 2))) &lt; current-date()]]"/>
    <xsl:template match="prices">
        <xsl:element name="{name()}">
            <xsl:choose>
                <xsl:when test="(price[@order_type = 'A']/year | price[@order_type = 'A']/@referred_year) = year-from-date(current-date())">
                    <!-- wenn Preise = aktueles jahr -->
                    <xsl:apply-templates select="price[(year, @referred_year) = year-from-date(current-date()) and @order_type = 'A' and price_type = ('ONL', 'PRIPRE', 'PRIONL', 'IND', 'STUD', 'EGP', 'UPD', 'REC', 'REI', 'U1', 'IFCP')]"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- kein Preis gleich aktuelles Jahr -->
                    <xsl:variable name="max_year" select="max((max(price[price_type != 'IFCP']/year), max(price[price_type != 'IFCP']/@referred_year)))"/>
                    <xsl:apply-templates select="price[price_type != 'IFCP'][(year, @referred_year) = $max_year][@order_type = 'A']"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    <xsl:template match="price">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="web-type">
                <xsl:choose>
                    <xsl:when test="price_type = ('IND', 'U1')">shop</xsl:when>
                    <xsl:when test="price_type = ('IFCP')">chunk</xsl:when>
                    <xsl:otherwise>display</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:copy-of select="*"/>
        </xsl:element>
    </xsl:template>
    <!-- combine texts add xml:lang attribute -->
    <xsl:template match="contents/text[@text_type = ('HEAD', 'HEAE', 'PLUD', 'PLUE', 'HGRD', 'HGRE', 'FOAD', 'FOAE', 'VORD', 'VORE', 'AJGD', 'AJGE', 'EDBD', 'EDBE', 'HIND', 'HINE','BUPD','BUPE','SKD','SKE')]">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="substring(@text_type, 1, string-length(@text_type) - 1)"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang">
                <xsl:choose>
                    <xsl:when test="ends-with(@text_type, 'D')">de</xsl:when>
                    <xsl:when test="ends-with(@text_type, 'E')">en</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'INDE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'IN'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'INEN']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'IN'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'HIST']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'HIST'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'HISE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'HIST'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'ABST']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'ABST'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'ABSE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'ABST'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'IMFA']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'IMFA'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'IMFE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'IMFA'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'ZIGR']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'ZIGR'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'ZIGE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'ZIGR'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'PURE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'PURE'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'PURU']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'PURE'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'NACH']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'NACH'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'NACE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'NACH'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'HWFA']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'HWFA'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'HWFE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'HWFA'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'SWTD']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'SW'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'SWTE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'SW'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'GUIG']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'GUI'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'GUIE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'GUI'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'UPFG']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'UPF'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'de'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="contents/text[@text_type = 'UPFE']">
        <xsl:element name="{name()}">
            <xsl:copy-of select="@text_status"/>
            <xsl:attribute name="text_type" select="'UPF'"/>
            <xsl:copy-of select="@term"/>
            <xsl:attribute name="xml:lang" select="'en'"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
